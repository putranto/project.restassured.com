package test.fdn;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

public class GetProductDetail {

    @BeforeMethod
    public void beforeMethod() {
        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
        requestSpecBuilder.setBaseUri("http://api-dev.femaledaily.net/app/v1");

        RestAssured.requestSpecification = requestSpecBuilder.build();
    }

    @Test
    public void validateStatusCodeResponse1314() {
        Response response = get("products/detail/1314").then().log().all().extract().response();
        assertThat(response.statusCode(), equalTo(200));
        //product id
        assertThat(response.path("data.prod_id").toString(), equalTo("1314"));
        //review id
        assertThat(response.path("data.id").toString(), equalTo("1316"));
        //product name
        assertThat(response.path("data.name").toString(), equalTo("Whitening Source Derm-Brightener"));
        //brand name
        assertThat(response.path("data.brand.name").toString(), equalTo("SK-II"));
    }

    @Test(dependsOnMethods = { "validateStatusCodeResponse1314" })
    public void validateStatusCodeResponse1315() {
        Response response = get("products/detail/1315").then().log().all().extract().response();
        assertThat(response.statusCode(), equalTo(200));
        //product id
        assertThat(response.path("data.prod_id").toString(), equalTo("1315"));
        //review id
        assertThat(response.path("data.id").toString(), equalTo("1317"));
        //product name
        assertThat(response.path("data.name").toString(), equalTo("Light Complete Milky Lightening Dew Toner"));
        //brand name
        assertThat(response.path("data.brand.name").toString(), equalTo("Garnier"));
    }

    @Test(dependsOnMethods = { "validateStatusCodeResponse1315" })
    public void validateStatusCodeResponse1317() {
        Response response = get("products/detail/1317").then().log().all().extract().response();
        assertThat(response.statusCode(), equalTo(200));
        //product id
        assertThat(response.path("data.prod_id").toString(), equalTo("1317"));
        //review id
        assertThat(response.path("data.id").toString(), equalTo("1319"));
        //product name
        assertThat(response.path("data.name").toString(), equalTo("Stay-Matte Sheer Pressed Powder"));
        //brand name
        assertThat(response.path("data.brand.name").toString(), equalTo("Clinique"));
    }
}
